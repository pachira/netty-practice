package top.fomeiherz.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

/**
 * 客户端进程
 */
public class SocketClient {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 8888);
        System.out.println(String.format("Time: %s, client startup.", new Date()));
        InputStream is = socket.getInputStream();
        Scanner scanner = new Scanner(is);
        // 有写入
        while (scanner.hasNext()) {
            String content = scanner.next();
            System.out.println("Time: " + new Date() + ", " + content);
        }

        is.close();
        socket.close();
    }

}
