package top.fomeiherz.io;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 服务器端进程
 */
public class BioServer {

    public static void main(String[] args) throws IOException {
        // 单线程
//        single();
        // 多线程处理
//        multiple();
        // 多线程处理（休眠10s）
//        multipleBlockingSync();
        // 多线程处理（接收连接和业务处理分离）
//        multipleBlockingSyncWithPerThread();
        // 线程池处理（接收连接和业务处理分离）
        multipleBlockingSyncWithExecutor();
    }

    /**
     * 只接收单个客户端
     *
     * @throws IOException
     */
    private static void single() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        // 此处会一直阻塞，直到有新的Socket连接
        Socket socket = serverSocket.accept();
        OutputStream os = socket.getOutputStream();
        // 传输数据
        os.write("Hei!".getBytes());

        os.close();
        socket.close();
        serverSocket.close();
    }

    private static void multiple() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            // 此处会一直阻塞，直到有新的Socket连接
            Socket socket = serverSocket.accept();
            OutputStream os = socket.getOutputStream();
            // 传输数据
            os.write("Hei!".getBytes());
            os.close();
            socket.close();
        }
        // serverSocket.close();
    }

    /**
     * 【阻塞同步IO】
     * 一次只能建立一个客户端连接，并且等待该客户端的业务处理完毕后才接收新的连接。
     *
     * @throws IOException
     */
    private static void multipleBlockingSync() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            // 此处会一直阻塞，直到有新的Socket连接
            Socket socket = serverSocket.accept();
            System.out.println(String.format("Time: %s, Have a new connection.", new Date()));
            OutputStream os = socket.getOutputStream();
            // 传输数据
            os.write("Hei!".getBytes());
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            os.close();
            socket.close();
        }
        // serverSocket.close();
    }

    /**
     * 【阻塞同步IO】
     * 建立连接和业务处理分开。服务端与客户端建立连接后，交给一个线程去处理接下来的业务。一次可以同时建立多个客户端连接。
     *
     * @throws IOException
     */
    private static void multipleBlockingSyncWithPerThread() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            // 此处会一直阻塞，直到有新的Socket连接
            final Socket socket = serverSocket.accept();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println(String.format("Time: %s, Have a new connection.", new Date()));
                        OutputStream os = socket.getOutputStream();
                        // 传输数据
                        os.write("Hei!".getBytes());
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        os.close();
                        socket.close();
                    } catch (IOException ex) {

                    }
                }
            }).start();
        }
        // serverSocket.close();
    }

    /**
     * 【阻塞同步IO】
     * 建立连接和业务处理分开。服务端与客户端建立连接后，交给线程池去处理接下来的业务。一次可以同时建立多个客户端连接。
     *
     * @throws IOException
     */
    private static void multipleBlockingSyncWithExecutor() throws IOException {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            // 此处会一直阻塞，直到有新的Socket连接
            final Socket socket = serverSocket.accept();
            executorService.submit(new Runnable() {
                public void run() {
                    try {
                        System.out.println(String.format("Time: %s, Have a new connection.", new Date()));
                        OutputStream os = socket.getOutputStream();
                        // 传输数据
                        os.write("Hei!".getBytes());
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        os.close();
                        socket.close();
                    } catch (IOException ex) {

                    }
                }
            });
        }
        // serverSocket.close();
    }

}
