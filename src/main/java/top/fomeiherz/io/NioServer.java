package top.fomeiherz.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * NIO-非阻塞IO，读、写、连接accept()事件都是阻塞的。{@link BioServer}在等待客户端连接（accept()方法）时会阻塞等待。
 *
 * 问题：启动{@link SocketClient}后，直到关闭该类，{@link SocketClient}才会输出服务端写入的内容？
 */
public class NioServer {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.bind(new InetSocketAddress(8888));
        // 设置服务端非阻塞
        serverSocket.configureBlocking(false);

        Selector selector = Selector.open();
        // 注册OP_ACCEPT事件到ServerSocketChannel
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            // 阻塞等待客户端OP_ACCEPT事件
            selector.select();

            // 获取所有被触发的事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();

            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();

                // 客户端OP_ACCEPT事件被触发了
                if (selectionKey.isAcceptable()) {
                    System.out.println("Ready to accept a new socket.");
                    ServerSocketChannel serverSocketChannel = (ServerSocketChannel) selectionKey.channel();
                    // 接收客户端Channel连接
                    SocketChannel clientChannel = serverSocketChannel.accept();
                    clientChannel.configureBlocking(false);
                    // 客户端Channel注册OP_WRITE事件
                    clientChannel.register(selector, SelectionKey.OP_WRITE);
                } else if (selectionKey.isWritable()) {
                    System.out.println("Ready for writing.");
                    // 注：这里是客户端Channel，使用客户端Channel注册OP_WRITE事件
                    SocketChannel clientChannel = (SocketChannel) selectionKey.channel();

                    String data = "Hei!";
                    ByteBuffer buffer = ByteBuffer.allocate(data.length());
                    buffer.put(data.getBytes());
                    buffer.flip();
                    // 写入数据
                    clientChannel.write(buffer);
                }
                selectionKey.cancel();
                //iterator.remove();
            }
        }
    }

}
